/*******************************************************************************
 * Copyright (c) 2017, RISE SICS AB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package se.sics.ace.coap.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Base64;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.elements.Connector;
import org.eclipse.californium.elements.UDPConnector;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.dtls.cipher.CipherSuite;
import org.eclipse.californium.scandium.dtls.pskstore.InMemoryPskStore;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;

import com.upokecenter.cbor.CBORObject;

import COSE.CoseException;
import COSE.KeyKeys;
import COSE.OneKey;
import se.sics.ace.AceException;

/**
 * Implements getting a token from the /token endpoint for a client 
 * using the DTLS profile.
 * 
 * Also implements POSTing the token to the /authz-info endpoint at the 
 * RS.
 * 
 * Clients are expected to create an instance of this class when the want to
 * perform token requests from a specific AS.
 * 
 * @author Ludwig Seitz
 *
 */
public class DTLSProfileRequests {
    
    /**
     * The logger
     */
    private static final Logger LOGGER 
        = Logger.getLogger(DTLSProfileRequests.class.getName() ); 

    /**
     * Sends a POST request to the /token endpoint of the AS to request an
     * access token. If the DTLS connection uses pre-shared symmetric keys 
     * we will use the key identifier (COSE kid) as psk_identity.
     * 
     * @param asAddr  the full address of the /token endpoint
     *  (including scheme and hostname, and port if not default)
     * @param payload  the payload of the request.  Use the GetToken 
     *  class to construct this payload
     * @param key  the key to be used to secure the connection to the AS. 
     *  This MUST have a kid.
     * 
     * @return  the response 
     *
     * @throws AceException 
     */
    public static CoapResponse getToken(String asAddr, CBORObject payload, 
            OneKey key) throws AceException {
        DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(
                new InetSocketAddress(0));
        builder.setClientAuthenticationRequired(true);
        builder.setClientOnly();
        CBORObject type = key.get(KeyKeys.KeyType);
        if (type.equals(KeyKeys.KeyType_Octet)) {
            String keyId = Base64.getEncoder().encodeToString(
                    key.get(KeyKeys.KeyId).GetByteString());
            builder.setPskStore(new StaticPskStore(
                    keyId, key.get(KeyKeys.Octet_K).GetByteString()));
            builder.setSupportedCipherSuites(new CipherSuite[]{
                    CipherSuite.TLS_PSK_WITH_AES_128_CCM_8});
        } else if (type.equals(KeyKeys.KeyType_EC2)){
            try {
                builder.setIdentity(key.AsPrivateKey(), key.AsPublicKey());
            } catch (CoseException e) {
                LOGGER.severe("Failed to transform key: " + e.getMessage());
                throw new AceException(e.getMessage());
            }
            builder.setSupportedCipherSuites(new CipherSuite[]{
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8});
        } else {
            LOGGER.severe("Unknwon key type used for getting a token");
            throw new AceException("Unknown key type");
        }

        DTLSConnector dtlsConnector = new DTLSConnector(builder.build());      
        CoapEndpoint ep = new CoapEndpoint(dtlsConnector, 
                NetworkConfig.getStandard());
        CoapClient client = new CoapClient(asAddr);
        client.setEndpoint(ep);
        try {
            dtlsConnector.start();
        } catch (IOException e) {
            LOGGER.severe("Failed to start DTLSConnector: " + e.getMessage());
            throw new AceException(e.getMessage());
        }
        return client.post(
                payload.EncodeToBytes(), 
                MediaTypeRegistry.APPLICATION_CBOR);
    }
    
    /**
     * Sends a POST request to the /authz-info endpoint of the RS to submit an
     * access token.
     * 
     * @param rsAddr  the full address of the /authz-info endpoint
     *  (including scheme and hostname, and port if not default)
     * @param payload  the token received from the getToken() method
     * @param key  an asymmetric key-pair to use with DTLS in a raw-public 
     *  key handshake
     * 
     * @return  the response 
     *
     * @throws AceException 
     */
    public static CoapResponse postToken(String rsAddr, CBORObject payload, 
            OneKey key) throws AceException {
        if (payload == null) {
            throw new AceException(
                    "Payload cannot be null when POSTing to authz-info");
        }
        Connector c = null;
        if (key != null) {
            DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(
                    new InetSocketAddress(0));
            builder.setClientAuthenticationRequired(true);
            builder.setClientOnly();
            builder.setSupportedCipherSuites(new CipherSuite[]{
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8});
            try {
                builder.setIdentity(key.AsPrivateKey(), key.AsPublicKey());
            } catch (CoseException e) {
                LOGGER.severe("Key is invalid: " + e.getMessage());
               throw new AceException("Aborting, key invalid: " 
                       + e.getMessage());
            }
            c = new DTLSConnector(builder.build());
        } else {
            c = new UDPConnector();
        }
        try {
            c.start();
        } catch (IOException e) {
            LOGGER.severe("Failed to start DTLSConnector: " + e.getMessage());
            throw new AceException(e.getMessage());
        }
        CoapEndpoint e = new CoapEndpoint(c, NetworkConfig.getStandard());
        CoapClient client = new CoapClient(rsAddr);
        client.setEndpoint(e);   
        LOGGER.finest("Sending request payload: " + payload);
        return client.post(
                payload.EncodeToBytes(), 
                MediaTypeRegistry.APPLICATION_CBOR);
    }
    
    
    /**
     * Generates a Coap client for sending requests to an RS that will pass the
     *  access token through psk-identity in the DTLS handshake.
     * @param serverAddress  the address of the server and resource this client
     *  should talk to
     * @param token  the access token this client should use towards the server
     * @param key  the pre-shared key for use with this server.
     * 
     * @return  a CoAP client configured to pass the access token through the
     *  psk-identity in the handshake 
     */
    public static CoapClient getPskClient(InetSocketAddress serverAddress,
            CBORObject token, OneKey key) {
        if (serverAddress == null || serverAddress.getHostName() == null) {
            throw new IllegalArgumentException(
                    "Client requires a non-null server address");
        }
        if (token == null) {
            throw new IllegalArgumentException(
                    "PSK client requires a non-null access token");
        }
        if (key == null || key.get(KeyKeys.Octet_K) == null) {
            throw new IllegalArgumentException(
                    "PSK  client requires a non-null symmetric key");
        }
        
        DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(
                new InetSocketAddress(0));
        builder.setClientAuthenticationRequired(true);
        builder.setClientOnly();
        builder.setSupportedCipherSuites(new CipherSuite[]{
                CipherSuite.TLS_PSK_WITH_AES_128_CCM_8});
        
        InMemoryPskStore store = new InMemoryPskStore();
       
        String identity = Base64.getEncoder().encodeToString(
                token.EncodeToBytes());
        LOGGER.finest("Adding key for: " + serverAddress.toString());
        store.addKnownPeer(serverAddress, identity, 
                key.get(KeyKeys.Octet_K).GetByteString());
        builder.setPskStore(store);
        Connector c = new DTLSConnector(builder.build());
        CoapEndpoint e = new CoapEndpoint(c, NetworkConfig.getStandard());
        CoapClient client = new CoapClient(serverAddress.getHostName());
        client.setEndpoint(e);   

        return client;    
    }
    
    
    /**
     * Generates a Coap client for sending requests to an RS that will pass the
     *  kid of a known access token through psk-identity in the DTLS handshake.
     * @param serverAddress  the address of the server and resource this client
     *  should talk to
     * @param kid  the kid that the client should use as PSK in the handshake
     * @param key  the pre-shared key for use with this server.
     * 
     * @return  a CoAP client configured to pass the access token through the
     *  psk-identity in the
     *  handshake 
     */
    public static CoapClient getPskClient(InetSocketAddress serverAddress,
            byte[] kid, OneKey key) {
        if (serverAddress == null || serverAddress.getHostName() == null) {
            throw new IllegalArgumentException(
                    "Client requires a non-null server address");
        }
        if (kid == null) {
            throw new IllegalArgumentException(
                    "PSK client requires a non-null kid");
        }
        if (key == null || key.get(KeyKeys.Octet_K) == null) {
            throw new IllegalArgumentException(
                    "PSK  client requires a non-null symmetric key");
        }
        
        DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(
                new InetSocketAddress(0));
        builder.setClientAuthenticationRequired(true);
        builder.setClientOnly();
        builder.setSupportedCipherSuites(new CipherSuite[]{
                CipherSuite.TLS_PSK_WITH_AES_128_CCM_8});
        
        InMemoryPskStore store = new InMemoryPskStore();

        String identity = Base64.getEncoder().encodeToString(kid);
        LOGGER.finest("Adding key for: " + serverAddress.toString());
        store.addKnownPeer(serverAddress, identity, 
                key.get(KeyKeys.Octet_K).GetByteString());
        builder.setPskStore(store);
        Connector c = new DTLSConnector(builder.build());
        CoapEndpoint e = new CoapEndpoint(c, NetworkConfig.getStandard());
        CoapClient client = new CoapClient(serverAddress.getHostName());
        client.setEndpoint(e);   

        return client;    
    }
    
    /**
     * Generates a Coap client for sending requests to an RS that will use
     * a raw public key to connect to the server.
     * 
     * @param clientKey  the raw asymmetric key of the client
     * @param rsPublicKey  the raw public key of the RS
     * @return   the CoAP client
     * @throws CoseException 
     */
    public static CoapClient getRpkClient(OneKey clientKey, OneKey rsPublicKey) 
            throws CoseException {
        DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder(
                new InetSocketAddress(0));
        builder.setClientAuthenticationRequired(true);
        builder.setClientOnly();
        builder.setSupportedCipherSuites(new CipherSuite[]{
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8});
        builder.setIdentity(clientKey.AsPrivateKey(), clientKey.AsPublicKey());

        Connector c = new DTLSConnector(builder.build());
        CoapEndpoint e = new CoapEndpoint(c, NetworkConfig.getStandard());
        CoapClient client = new CoapClient();
        client.setEndpoint(e);   
        
        return client;    
    }    
}
